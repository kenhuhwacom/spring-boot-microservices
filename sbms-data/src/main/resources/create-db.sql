-- 
-- create / re-create entire database
--
drop database if exists ${jdbc.schema};
create database ${jdbc.schema} default character set utf8;
