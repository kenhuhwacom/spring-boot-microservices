--
-- create database user, shall only execute one time
-- 
grant all on ${jdbc.schema}.* to ${jdbc.username} identified by '${jdbc.password}';

-- new line is necessary since '@' will break maven filtering
grant all on ${jdbc.schema}.* to ${jdbc.username}@'localhost' 
    identified by '${jdbc.password}';
