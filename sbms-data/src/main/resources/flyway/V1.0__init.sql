drop table if exists hibernate_sequence;
drop table if exists library_book;
drop table if exists library_user;
drop table if exists rented_record;

create table hibernate_sequence (next_val bigint);
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );

create table library_book (id bigint not null, isbn varchar(255), title varchar(255), rentedRecord_id bigint, primary key (id));
create table library_user (id bigint not null, account varchar(255), age integer not null, email varchar(255), name varchar(255), primary key (id));
create table rented_record (id bigint not null, user_id bigint, primary key (id));
alter  table library_book add constraint FK4701xdnomom69rhl9a782l7n8 foreign key (rentedRecord_id) references rented_record (id);