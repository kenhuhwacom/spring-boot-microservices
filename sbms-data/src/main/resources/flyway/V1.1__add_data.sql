insert into library_user (id,account,age,email,name) values (100, 'ken','48','ken.hu@hwacom.com','Ken Hu');
insert into library_user (id,account,age,email,name) values (101, 'chiayi','35','chiayi.chen@hwacom.com','Chiayi Chen');
insert into library_user (id,account,age,email,name) values (102, 'jonathan','35','jonathan.tsai@hwacom.com','Jonathan Tsai');
insert into library_user (id,account,age,email,name) values (103, 'hsinyu','29','hsinyu.yang@hwacom.com','Hsinyu Yang');

insert into rented_record (id,user_id) values (200, 100);
insert into rented_record (id,user_id) values (201, 101);
insert into rented_record (id,user_id) values (202, 102);
insert into rented_record (id,user_id) values (203, 103);

insert into library_book (id,isbn,title,rentedRecord_id) values (300, '9781423168898', 'The Little Mermaid', 200);
insert into library_book (id,isbn,title,rentedRecord_id) values (301, '9781302914332', 'Amazing Spider-Man 3: Lifetime Achievement', 201);
insert into library_book (id,isbn,title,rentedRecord_id) values (302, '9781302903206', 'Invincible Iron Man 3: Civil War II', 202);
insert into library_book (id,isbn,title,rentedRecord_id) values (303, '9781302911942', 'Captain America 1: Winter in America', 203);
insert into library_book (id,isbn,title) values (304, '9781302912567', 'The Immortal Hulk 2: The Green Door');

update hibernate_sequence set next_val=500;