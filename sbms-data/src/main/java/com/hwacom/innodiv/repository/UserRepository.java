package com.hwacom.innodiv.repository;

import com.hwacom.innodiv.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for Book entity.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByName(String name);

    User findUserByAccount(String account);
}
