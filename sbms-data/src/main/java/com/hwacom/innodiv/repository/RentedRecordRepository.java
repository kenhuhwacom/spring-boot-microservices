package com.hwacom.innodiv.repository;

import com.hwacom.innodiv.model.RentedRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RentedRecordRepository extends JpaRepository<RentedRecord, Long> {

    List<RentedRecord> findRentedRecordsByUserId(Long userId);
}
