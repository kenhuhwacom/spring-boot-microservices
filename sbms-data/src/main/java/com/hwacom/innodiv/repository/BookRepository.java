package com.hwacom.innodiv.repository;

import com.hwacom.innodiv.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Jpa repository interface for entity Book.
 */
public interface BookRepository extends JpaRepository<Book, Long> {
    @Query("SELECT b FROM Book b WHERE b.rentedRecord IN (SELECT r FROM RentedRecord r WHERE r"
        + ".userId = ?1)")
    List<Book> findBooksByUserId(Long userId);
}
