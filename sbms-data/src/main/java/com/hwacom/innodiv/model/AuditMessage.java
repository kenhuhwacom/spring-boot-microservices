package com.hwacom.innodiv.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 系統稽核訊息資料類別.
 */
@Getter
@Setter
@ToString
public class AuditMessage {
    private String title;
    private String message;
}
