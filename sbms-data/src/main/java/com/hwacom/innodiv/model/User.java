package com.hwacom.innodiv.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * 使用者資料類別.
 */
@Entity
@Table(name = "library_user")
@Data
public class User {
    /**
     * 使用者Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * 使用者帳號.
     */
    private String account;
    /**
     * 使用者姓名.
     */
    private String name;
    /**
     * 使用者email地址.
     */
    private String email;
    /**
     * 使用者年齡.
     */
    private int age;
    /**
     * 目前借閱書籍.
     */
    @Transient
    private List<Book> rentedBooks;
}
