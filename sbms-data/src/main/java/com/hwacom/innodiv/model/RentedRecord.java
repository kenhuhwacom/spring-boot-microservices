package com.hwacom.innodiv.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "rented_record")
@Data
public class RentedRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(mappedBy = "rentedRecord", fetch = FetchType.EAGER)
    // prevent infinite recursion
    @JsonManagedReference
    private Collection<Book> rentedBooks;
    @Column(name = "user_id")
    private Long userId;
}
