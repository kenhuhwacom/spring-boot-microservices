package com.hwacom.innodiv.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 圖書館藏書.
 */
@Entity
@Table(name = "library_book")
@Data
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    private String isbn;
    @ManyToOne(fetch = FetchType.EAGER)
    // prevent infinite recursion
    @JsonBackReference
    private RentedRecord rentedRecord;
}
