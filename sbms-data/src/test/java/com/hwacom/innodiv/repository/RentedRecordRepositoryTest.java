package com.hwacom.innodiv.repository;

import com.hwacom.innodiv.model.Book;
import com.hwacom.innodiv.model.RentedRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserRepositoryTest.JpaTestConfiguration.class)
@Transactional
public class RentedRecordRepositoryTest {

    @SpringBootApplication
    @EntityScan(basePackages = "com.hwacom.innodiv.model")
    @EnableJpaRepositories(basePackages = "com.hwacom.innodiv.repository")
    @EnableTransactionManagement
    static class JpaTestConfiguration {
    }

    @Resource
    private RentedRecordRepository rentedRecordRepository;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testRentedRecordCrudOperations() {
        Book book1 = new Book();
        book1.setTitle("The Little Mermaid");
        book1.setIsbn("9781423168898");

        Book book2 = new Book();
        book2.setTitle("Amazing Spider-Man 3: Lifetime Achievement");
        book2.setIsbn("9781302914332");

        RentedRecord record = new RentedRecord();
        record.setUserId(100L);
        record.setRentedBooks(Arrays.asList(book1, book2));

        rentedRecordRepository.save(record);

        List<RentedRecord> recordsInDb = rentedRecordRepository.findRentedRecordsByUserId(100L);
        assertEquals(1, recordsInDb.size());
        assertEquals(2, recordsInDb.get(0).getRentedBooks().size());
        recordsInDb.get(0).getRentedBooks().stream().forEach(b -> System.out.println(b.getTitle()));
    }
}