package com.hwacom.innodiv.repository;

import com.hwacom.innodiv.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.sql.DataSource;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserRepositoryTest.JpaTestConfiguration.class)
@Transactional
public class UserRepositoryTest {

    @SpringBootApplication
    @EntityScan(basePackages = "com.hwacom.innodiv.model")
    @EnableJpaRepositories(basePackages = "com.hwacom.innodiv.repository")
    @EnableTransactionManagement
    static class JpaTestConfiguration {
    }

    @Resource
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testUserCrudOperations() {
        User user = new User();
        user.setAge(72);
        user.setEmail("trump.president@gov");
        user.setAccount("donald.trump");
        user.setName("Donald Trump");

        userRepository.save(user);

        User userInDb = userRepository.findUserByName("Donald Trump");
        assertNotNull(userInDb);
        assertEquals(72, userInDb.getAge());

        userInDb = userRepository.findUserByAccount("donald.trump");
        assertNotNull(userInDb);
        assertEquals("donald.trump", userInDb.getAccount());
    }
}