package com.hwacom.innodiv.repository;

import com.hwacom.innodiv.model.Book;
import com.hwacom.innodiv.model.RentedRecord;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookRepositoryTest.JpaTestConfiguration.class)
@Transactional
public class BookRepositoryTest {
    @Autowired
    private RentedRecordRepository rentedRecordRepository;

    @Autowired
    private BookRepository bookRepository;

    @SpringBootApplication
    @EntityScan(basePackages = "com.hwacom.innodiv.model")
    @EnableJpaRepositories(basePackages = "com.hwacom.innodiv.repository")
    @EnableTransactionManagement
    static class JpaTestConfiguration {
    }

    @Before
    public void setUp() throws Exception {
        Book book1 = new Book();
        book1.setTitle("The Little Mermaid");
        book1.setIsbn("9781423168898");

        Book book2 = new Book();
        book2.setTitle("Amazing Spider-Man 3: Lifetime Achievement");
        book2.setIsbn("9781302914332");

        bookRepository.save(book1);

        RentedRecord record = new RentedRecord();
        record.setUserId(100L);
        record.setRentedBooks(Arrays.asList(book2));

        rentedRecordRepository.save(record);

        book2.setRentedRecord(record);
        bookRepository.save(book2);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBookCrudOperations() {
        List<Book> booksInDb = bookRepository.findAll();
        assertEquals(2, booksInDb.size());

        Optional<Book> book1InDb = bookRepository.findById(booksInDb.get(0).getId());
        assertTrue(book1InDb.isPresent());
        Optional<Book> book2InDb = bookRepository.findById(booksInDb.get(1).getId());
        assertTrue(book2InDb.isPresent());
    }

    @Test
    public void testFindBooksByUser() {
        List<Book> booksInDb = bookRepository.findBooksByUserId(100L);
        // only book2
        assertEquals(1, booksInDb.size());
        assertEquals("Amazing Spider-Man 3: Lifetime Achievement", booksInDb.get(0).getTitle());
        assertEquals("9781302914332", booksInDb.get(0).getIsbn());
    }
}