package com.hwacom.innodiv.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hwacom.innodiv.model.User;
import com.hwacom.innodiv.repository.BookRepository;
import com.hwacom.innodiv.repository.RentedRecordRepository;
import com.hwacom.innodiv.repository.UserRepository;
import com.hwacom.innodiv.user.UserApplication;
import com.hwacom.innodiv.user.service.UserService;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.Repository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties={
    "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration, org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration, org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration, org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration, org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration"})
public class UserControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;
    @MockBean
    private UserService userService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private RentedRecordRepository rentedRecordRepository;
    @MockBean
    private BookRepository bookRepository;

    /**
     * Jackson object mapper.
     */
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testGetUser() {
        User trump = new User();
        trump.setId(100L);
        trump.setName("Donald Trump");
        trump.setAge(72);
        trump.setRentedBooks(Lists.emptyList());

        when(userService.findUser(any())).thenReturn(trump);
        ResponseEntity entity = restTemplate.getForEntity("/api/users/trump", User.class);
        verify(userService, times(1)).findUser(any(String.class));
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }
}