package com.hwacom.innodiv.user.service;

import com.hwacom.innodiv.model.AuditMessage;
import com.hwacom.innodiv.model.Book;
import com.hwacom.innodiv.model.User;
import com.hwacom.innodiv.repository.UserRepository;
import com.hwacom.innodiv.user.mq.AuditSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User服務類別.
 */
@Service
@EnableBinding(AuditSource.class)
public class UserService {
    @Autowired
    private BookService bookService;
    @Autowired
    private AuditSource auditSource;
    @Autowired
    private UserRepository userRepository;
    /**
     * Find user by id.
     * @param account user id
     * @return an User
     * @throws IllegalStateException if user not found
     */
    public User findUser(String account) {
        User user = userRepository.findUserByAccount(account);

        if (user != null) {
            List<Book> rentedBooks = bookService.findRentedBooks(user.getId());
            user.setRentedBooks(rentedBooks);
            //send message to broker
            auditSource.auditLogging().send(MessageBuilder.withPayload(
                buildAuditMessage(user)).build());
            return user;
        } else {
            throw new IllegalStateException(String.format("user %s not found", account));
        }
    }

    private AuditMessage buildAuditMessage(User user) {
        AuditMessage mesg = new AuditMessage();
        mesg.setTitle(String.format("User %s is found", user.getName()));
        mesg.setMessage(user.toString());
        return mesg;
    }
}
