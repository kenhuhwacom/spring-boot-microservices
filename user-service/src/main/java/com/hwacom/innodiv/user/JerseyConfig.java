package com.hwacom.innodiv.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.ws.rs.ApplicationPath;
import com.hwacom.innodiv.user.controller.UserController;

/**
 * Jersey設定類別.
 */
@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {
    /**
     * Default constructor.
     * @param objectMapper ObjectMapper instance
     */
    @Autowired
    public JerseyConfig(ObjectMapper objectMapper) {

        // register endpoints
	// use packages scan in Spring Boot Standalone JAR will fails
        // packages("com.hwacom.innodiv.user.controller");
	register(UserController.class);

        // register jackson for json
        // register(new ObjectMapperContextResolver(objectMapper));


    }
}
