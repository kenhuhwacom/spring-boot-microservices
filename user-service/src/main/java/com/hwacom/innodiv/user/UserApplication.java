package com.hwacom.innodiv.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Main class to bootstrap and start spring boot application.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EntityScan(basePackages = "com.hwacom.innodiv.model")
@EnableJpaRepositories(basePackages = {"com.hwacom.innodiv.repository"})
public class UserApplication {

    /**
     * Old main method.
     * @param args console arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

}
