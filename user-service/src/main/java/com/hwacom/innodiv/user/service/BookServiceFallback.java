package com.hwacom.innodiv.user.service;

import com.hwacom.innodiv.model.Book;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 在circuit breaker斷路時,用這個實作類別來提供暫時性的服務.
 */
@Service
public class BookServiceFallback implements BookService {
    @Override
    public Book findBookById(Long id) {
        Book fakeBook = new Book();
        fakeBook.setId(0L);
        fakeBook.setIsbn("");
        fakeBook.setTitle("Unknown");

        return fakeBook;
    }

    @Override
    public List<Book> findRentedBooks(Long userId) {
        return new ArrayList<>();
    }
}
