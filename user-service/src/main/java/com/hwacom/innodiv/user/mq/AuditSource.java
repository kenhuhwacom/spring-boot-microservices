package com.hwacom.innodiv.user.mq;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * 連接MQ的訊息來源類別.
 */
public interface AuditSource {
    /**
     * 定義一個channel.
     * @return a MessageChannel instance
     */
    @Output("auditChannel")
    MessageChannel auditLogging();
}
