package com.hwacom.innodiv.user.controller;

import com.hwacom.innodiv.model.User;
import com.hwacom.innodiv.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * User相關的rest controller.
 */
@Component
@Path("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GET
    @Path("/{account}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("account") String account) {
        try {
            User user = userService.findUser(account);
            return Response.ok().entity(user).build();
        } catch (IllegalStateException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
