package com.hwacom.innodiv.user.service;

import com.hwacom.innodiv.model.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * Spring cloud Feign 客戶端.
 */
@Service
@FeignClient(value = "book-service", fallback = BookServiceFallback.class)
public interface BookService {
    @GetMapping("/api/books/{id}")
    Book findBookById(@PathVariable("id") Long id);

    @GetMapping("/api/books/user/{user_id}")
    List<Book> findRentedBooks(@PathVariable("user_id") Long userId);
}
