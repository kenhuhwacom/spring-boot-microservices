package com.hwacom.innodiv.servicediscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Main class to bootstrap and start spring boot application.
 */
@SpringBootApplication
@EnableEurekaServer
public class ServiceDiscoveryApplication {

    /**
     * Old main method.
     * @param args console arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(ServiceDiscoveryApplication.class, args);
    }

}
