package com.hwacom.innodiv.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Main class to bootstrap and start spring boot application.
 */
@SpringBootApplication(scanBasePackages = "com.hwacom.innodiv")
@EnableDiscoveryClient
@EntityScan(basePackages = "com.hwacom.innodiv.model")
@EnableJpaRepositories(basePackages = {"com.hwacom.innodiv.repository"})
public class BookApplication {

    /**
     * Old main method.
     * @param args console arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(BookApplication.class, args);
    }

}
