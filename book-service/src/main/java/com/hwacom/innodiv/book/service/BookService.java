package com.hwacom.innodiv.book.service;

import com.hwacom.innodiv.model.Book;
import com.hwacom.innodiv.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    @Transactional(readOnly = true)
    public Optional<Book> findBookById(Long bookId) {
        Optional<Book> book = bookRepository.findById(bookId);
        return book;
    }

    @Transactional(readOnly = true)
    public List<Book> findRentedBooksByUser(Long userId) {
        List<Book> books = bookRepository.findBooksByUserId(userId);
        return books;
    }
}
