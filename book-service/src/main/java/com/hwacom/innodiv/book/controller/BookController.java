package com.hwacom.innodiv.book.controller;

import com.hwacom.innodiv.book.service.BookService;
import com.hwacom.innodiv.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

/**
 * Greeting的rest controller.
 */
@Component
@Path("/books")
public class BookController {
    @Autowired
    private BookService bookService;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBook(@PathParam("id") Long bookId) {
        Optional<Book> book = bookService.findBookById(bookId);
        if (book.isPresent()) {
            GenericEntity<Book> entity = new GenericEntity<Book>(book.get(), Book.class);
            return Response.ok().entity(entity).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRentedBooks(@PathParam("id") Long userId) {
        List<Book> rentedBooks = bookService.findRentedBooksByUser(userId);
        return Response.ok(rentedBooks).build();
    }
}
