package com.hwacom.innodiv.book;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.ws.rs.ApplicationPath;
import com.hwacom.innodiv.book.controller.BookController;

/**
 * Jersey設定.
 */
@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {
    /**
     * Default constructor.
     * @param objectMapper objectMapper instance
     */
    @Autowired
    public JerseyConfig(ObjectMapper objectMapper) {
        /*
         use packages scan in Spring Boot Standalone JAR will fails, like:
         packages("com.hwacom.innodiv.book");
         */
        register(BookController.class);
    }
}
