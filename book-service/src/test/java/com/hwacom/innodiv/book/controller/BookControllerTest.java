package com.hwacom.innodiv.book.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hwacom.innodiv.book.service.BookService;
import com.hwacom.innodiv.model.Book;
import com.hwacom.innodiv.repository.BookRepository;
import com.hwacom.innodiv.repository.RentedRecordRepository;
import com.hwacom.innodiv.repository.UserRepository;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@TestPropertySource(properties={
    "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration, org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration, org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration, org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration, org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BookControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;
    @MockBean
    private BookService bookService;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private RentedRecordRepository rentedRecordRepository;
    @MockBean
    private UserRepository userRepository;
    /**
     * Jackson object mapper.
     */
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testGetBook() throws IOException {
        Book fakeBook = new Book();
        fakeBook.setId(100L);
        fakeBook.setIsbn("12345");
        fakeBook.setTitle("spiderman");
        when(bookService.findBookById(anyLong())).thenReturn(Optional.of(fakeBook));
        ResponseEntity<String> entity = this.restTemplate.getForEntity("/api/books/100",
            String.class);

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        Book bookReturned = mapper.readValue( entity.getBody(), Book.class);
        assertEquals(100, bookReturned.getId().intValue());
        assertEquals("12345", bookReturned.getIsbn());
        assertEquals("spiderman", bookReturned.getTitle());
    }

    @Test
    public void testGetBookNotFound() {
        when(bookService.findBookById(anyLong())).thenReturn(Optional.empty());
        ResponseEntity<String> entity = this.restTemplate.getForEntity("/api/books/100",
            String.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test
    public void testGetRentedBooks() throws IOException {
        Book book1 = new Book();
        book1.setId(100L);
        book1.setIsbn("67890");
        book1.setTitle("spiderman");

        Book book2 = new Book();
        book2.setId(200L);
        book2.setIsbn("12345");
        book2.setTitle("ironman");

        when(bookService.findRentedBooksByUser(anyLong())).thenReturn(Lists.list(book1, book2));
        ResponseEntity<String> entity = this.restTemplate.getForEntity("/api/books/user/300",
            String.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        List<Book> rentedBooks = mapper.readValue(entity.getBody(), new TypeReference<List<Book>>(){});
        assertEquals(2, rentedBooks.size());
        rentedBooks.sort((Book b1, Book b2)-> b1.getIsbn().compareTo(b2.getIsbn()));
        assertEquals("ironman", rentedBooks.get(0).getTitle());
        assertEquals("spiderman", rentedBooks.get(1).getTitle());
    }
}