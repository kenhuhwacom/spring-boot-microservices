package com.hwacom.innodiv.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * Main class to bootstrap and start spring boot application.
 */
@EnableBinding(Sink.class)
@SpringBootApplication
public class LoggingApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingApplication.class);

    /**
     * Old main method.
     * @param args console arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(LoggingApplication.class, args);
    }

    /**
     * 處理MQ上接收到的訊息.
     * @param auditMessage 稽核訊息
     */
    @StreamListener(target = Sink.INPUT)
    public void processAuditMessage(String auditMessage) {
        LOGGER.info("received audit message: " + auditMessage);
    }

}
