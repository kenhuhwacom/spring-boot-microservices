#!/bin/bash
services="cloud-config service-discovery book-service user-service logging-service api-gateway"
repository="kenhu1970"
tag="0.0.1"

if [ $# != 1 ]
then
    echo "Usage: $0 build|delete"
    exit
fi

command=$1

case "$command" in
  build)
    for service in $services
    do
      docker build -f Dockerfile -t $repository/ms-$service:$tag  --build-arg JAR_FILE=target/$service.jar ../$service
      sleep 2
    done
    ;;
  delete)
    for service in $services
    do
      docker rmi -f $repository/ms-$service:$tag
      sleep 2
    done
    ;;
  *)
    exit 1
esac

