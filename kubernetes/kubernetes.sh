#!/bin/bash
images="mariadb cloud-config book-service user-service api-gateway"
service_array=(rabbit-mq mariadb cloud-config book-service user-service api-gateway)
repository="kenhu1970"
tag="0.0.1"

if [ $# != 1 ]
then
    echo "Usage: $0 build|delete|deploy|undeploy"
    exit
fi

command=$1

case "$command" in
  build)
    echo "build docker images"
    for service in $images
    do
      if [ "$service" == "mariadb" ]
      then
        docker build -f $service/Dockerfile -t $repository/ms-$service:$tag ../$service
      else
        docker build -f $service/Dockerfile -t $repository/ms-$service:$tag --build-arg JAR_FILE=target/$service.jar ../$service
      fi
      sleep 2
    done
    ;;
  delete)
    echo "delete docker images"
    for service in $images
    do
      docker rmi -f $repository/ms-$service:$tag
      sleep 2
    done
    ;;
  deploy)
    echo "create k8s deployments and services"
    for service in "${service_array[@]}"
    do
      kubectl create -f "yaml/$service-deployment.yaml"
      sleep 3
      kubectl create -f "yaml/$service-service.yaml"
      sleep 3
    done
    ;;
  undeploy)
    echo "delete k8s deployments and services"
    for (( idx=${#service_array[@]}-1 ; idx>=0 ; idx-- ))
    do
      kubectl delete deployment "${service_array[idx]}" 
      sleep 3
      kubectl delete svc "${service_array[idx]}"
      sleep 3
    done
    ;;
  *)
    exit 1
esac

