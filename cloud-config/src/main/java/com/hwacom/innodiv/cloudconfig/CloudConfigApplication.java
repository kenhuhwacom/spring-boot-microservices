package com.hwacom.innodiv.cloudconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * Main class to bootstrap and start spring boot application.
 */
@SpringBootApplication
@EnableConfigServer
public class CloudConfigApplication {

    /**
     * Old main method.
     * @param args console arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(CloudConfigApplication.class, args);
    }

}
