# Microservices with Spring Boot

Use spring boot as our foundation to build microservices.

## requirements

- jdk: we use openjdk 1.8 for this project
- maven
- rabbitmq

## modules

- cloud-config: config server for configuration management, based on **spring cloud config** .
- service-discovery: register service instances and then find service instances to interact with, based on **spring cloud netflix eureka**.
- data: common data/model/entity shared by all modules.
- user-service: a demo service module based on **spring boot and Jersey(a JAX-RS implementation)**, and it is a message producer in this demo project.
- book-service: a demo service module based on **spring boot and Jersey(a JAX-RS implementation)**.
- api-gateway: a demo api gateway based on **spring cloud gateway**.
- logging: a demo message consumer for receiving and logging audit messages.

## system architecture

```
      +-------------+    +----------------+
      |Config Server|    |Service Registry|
      |1            |    |2               |
      +-------------+    +----------------+


      +-----------+      +---------+   +--------+   +--------+
      |API Gateway|      |Book     |   |User    |   |Logging |
      |           |      |Service  |   |Service |   |Service |
      |3          |      |4        |   |5       |   |6       |
      +-----------+      +---------+   +--------+   +--------+


      +---------+        1 <- 2, 3, 4, 5, 6
      |RabbitMQ |        2 <- 3, 4, 5
      |7        |        7 <- 5, 6 [auditlogs]
      +---------+        7 <- 1, 2, 3, 4, 5, 6 [spring cloud bus]
```

- Config Server(1) use bitbucket as its backend

- all module(2, 3, 4, 5, 6) get configurations from Config Server(1)

- all services(3, 4, 5, 6) register themselves to service registry(2) and then get other registrants from (2)

- users send request to API Gateway(3), and it forward this request to Book Service(4) or User Service(5)

- User Service(5) use synchronous http to call Book Service(4)

- When User Service(5) has been called, it publish a message to RabbitMQ(7)

- Logging Service(6) will be informed with an incoming message then save it in log

- use spring cloud bus (with RabbitMQ) to notify Config Server(1) and all module(2, 3, 4, 5, 6) to update configurations

## create database

- install mariadb or use maraidb docker instance in localhost

- make sure root can log in mariadb in localhost 

- create database and test data:

```
mvn -pl data -P fullInstall
```

- after that, a new database named sb_microservices and a database user named hwacom are created 

## test run

- in project folder: `mvn clean install`

- start rabbitmq in localhost

- start config server:

```
cd cloud-config
mvn spring-boot:run
```

- start eureka:

```
cd service-discovery
mvn spring-boot:run
```

- start user service:

```
cd user-service
mvn spring-boot:run
```

- start book service:

```
cd book-service
mvn spring-boot:run
```

- start logging service:

```
cd logging-service
mvn spring-boot:run
```

- start api gateway:

```
cd api-gateway
mvn spring-boot:run
```

## test scenarios

### get configurations from server and service:

```
wget http://localhost:9000/service-discovery/dev

wget http://localhost:9000/book-service/dev

wget http://localhost:9000/user-service/dev

wget http://localhost:9000/logging-service/dev

wget http://localhost:9000/api-gateway/dev
```

If config server use file backend, add "config-client" between base url and service property file name, ie:

```
wget http://localhost:9000/config-client/service-registry-dev.properties
```

### direct access services

- test user service:

```
wget http://localhost:9020/api/users/ken
```

- test book service:

```
wget http://localhost:9030/api/books/304
```

### access services via api gateway:

- test user service:

```
wget http://localhost:9040/users/ken
```

- test book service:

```
wget http://localhost:9040/books/304
```

### test message traffics

Everytime when user service is accessed (directly or indirectly), logging service will log this access activity.

### configurations update

- ask all participants to update their configurations, via spring cloud bus:

```
wget http://localhost:9000/bus/refresh
```

## dockerize

- until now (2019-06-20), we use a same Dockerfile for all modules, to build a docker image, take cloud-config module as example:

```
cd cloud-config
mvn package
docker build -f ../Dockerfile -t kenhu1970/cloud-config --build-arg JAR_FILE=target/cloud-config-0.0.1-SNAPSHOT.jar .
```

- run docker container, take cloud-config module as example:

```
docker run -d --net=host --rm kenhu1970/cloud-config
```

## references

- [spring cloud config documentation](https://cloud.spring.io/spring-cloud-config/single/spring-cloud-config.html)
- [spring cloud netflix eureka documentation](https://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/2.1.2.RELEASE/single/spring-cloud-netflix.html)
- [spring cloud gateway documentation](https://cloud.spring.io/spring-cloud-static/spring-cloud-gateway/2.1.0.RELEASE/single/spring-cloud-gateway.html)
- [spring cloud stream documentation](https://cloud.spring.io/spring-cloud-static/spring-cloud-stream/2.2.0.RELEASE/home.html)
- [spring cloud bus documentation](https://cloud.spring.io/spring-cloud-static/spring-cloud-bus/2.1.2.RELEASE/single/spring-cloud-bus.html)
- [Quick Guide to Microservices With Spring Boot 2.0, Eureka, and Spring Cloud](https://dzone.com/articles/quick-guide-to-microservices-with-spring-boot-20-e)
- [Using Swagger 2 with SpringFox and JAX-RS in a SpringBoot app](https://www.atechref.com/blog/spring-boot/using-swagger-2-with-spring-boot-spring-fox-and-jax-rs-project/)
